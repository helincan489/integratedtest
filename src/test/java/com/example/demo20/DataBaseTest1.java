package com.example.demo20;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.DynamicPropertyRegistry;
import org.springframework.test.context.DynamicPropertySource;
import org.testcontainers.containers.MySQLContainer;
import org.testcontainers.junit.jupiter.Container;
import org.testcontainers.junit.jupiter.Testcontainers;

@Testcontainers
@SpringBootTest
public class DataBaseTest1 {
  @Container
  private static MySQLContainer mySQLContainer=new MySQLContainer<>("mysql:8.0.36")
      .withDatabaseName("test")
      .withUsername("root")
      .withPassword("root");


  @DynamicPropertySource
  static void properties(DynamicPropertyRegistry registry)
  {
    registry.add("spring.datasource.url", mySQLContainer::getJdbcUrl);
    registry.add("spring.datasource.password", mySQLContainer::getPassword);
    registry.add("spring.datasource.username", mySQLContainer::getUsername);
  }

  @Test
  public void test1()
  {
    Assertions.assertTrue(mySQLContainer.isRunning());
  }
}
