package com.example.demo20;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;
import org.testcontainers.containers.GenericContainer;
import org.testcontainers.containers.MySQLContainer;
import org.testcontainers.junit.jupiter.Testcontainers;
import org.testcontainers.utility.DockerImageName;

@Testcontainers
@SpringBootTest
public class systemTest {
  private static MySQLContainer mySQLContainer;
  private static GenericContainer redisContainer;
  static {
    redisContainer = new GenericContainer<>(DockerImageName.parse("redis:6.0")).withExposedPorts(6379);
    redisContainer.start();
    System.setProperty("spring.redis.host", redisContainer.getHost());
    System.setProperty("spring.redis.port", redisContainer.getMappedPort(6379).toString());
    mySQLContainer=new MySQLContainer<>("mysql:8.0.36")
        .withDatabaseName("test")
        .withUsername("root")
        .withPassword("root");
    mySQLContainer.start();
    System.setProperty("spring.datasource.url", mySQLContainer.getJdbcUrl());
    System.setProperty("spring.datasource.password", mySQLContainer.getPassword());
    System.setProperty("spring.datasource.username", mySQLContainer.getUsername());
  }

  @Test
  public void test()
  {
    Assertions.assertTrue(mySQLContainer.isRunning());
    Assertions.assertTrue(redisContainer.isRunning());
  }
}
