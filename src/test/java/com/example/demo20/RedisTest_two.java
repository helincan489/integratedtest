package com.example.demo20;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.testcontainers.containers.GenericContainer;
import org.testcontainers.junit.jupiter.Testcontainers;
import org.testcontainers.utility.DockerImageName;

@Testcontainers
@SpringBootTest
public class RedisTest_two {
  static {
    GenericContainer redisContainer = new GenericContainer<>(DockerImageName.parse("redis:6.0")).withExposedPorts(6379);
    redisContainer.start();
    System.setProperty("spring.redis.host", redisContainer.getHost());
    System.setProperty("spring.redis.port", redisContainer.getMappedPort(6379).toString());
  }
  @Autowired
  private StringRedisTemplate stringRedisTemplate;

  @Test
  public void test1()
  {
    stringRedisTemplate.opsForValue().set("change","value");
  }
}
