package com.example.demo20;

import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.testcontainers.containers.GenericContainer;
import org.testcontainers.junit.jupiter.Testcontainers;
import org.testcontainers.utility.DockerImageName;

@Testcontainers
@SpringBootTest
@Slf4j
public class RedisTest_one {
  static {
    GenericContainer redisContainer = new GenericContainer<>(DockerImageName.parse("redis:6.0")).withExposedPorts(6379);
    redisContainer.start();
    System.setProperty("spring.redis.host", redisContainer.getHost());
    System.setProperty("spring.redis.port", redisContainer.getMappedPort(6379).toString());
  }

  @Autowired
  private StringRedisTemplate stringRedisTemplate;

  @Test
  public void test1()
  {
    stringRedisTemplate.opsForValue().set("kevin","hi!");
  }

  @Test
  public void test2()
  {
    log.info("hello: {}",stringRedisTemplate.opsForValue().get("kevin"));
  }

  @Test
  public void test3()
  {
    Assertions.assertEquals(stringRedisTemplate.opsForValue().get("kevin"),"hi!");
  }

}
